from ast import And
from flask import Flask
from flask_cors import CORS

import json

from helper import *
from link_prediction import *

app = Flask(__name__)
CORS(app)


@app.route("/original-network/<network>")
def load_network(network=None):
    print("START: load network " + network)
    if validateNetworkParam(network):
        (graph, _) = create_networkX_graphs(network)
        return build_json_object_of_graph(graph, {}, {}, network)
    else:
        return json.dumps({'error':'invalid network'})

@app.route("/explanations/<algorithm>")
def load_explanation(algorithm=None):
    print("START: oad explanation for " + algorithm)
    if validateAlgorithmParam(algorithm):
        title, subtitles, texts = read_explanation_file(algorithm)
        res = build_final_json_response_explanation(title, subtitles, texts)
        return res
    else:
        return json.dumps({'error':'invalid algorithm'})


@app.route("/start-algorithm/<algorithm>/<network>")
def start_algorithm(algorithm=None, network=None):
    print("START: start algorithm " + algorithm + " with network: " + network)
    if validateAlgorithmParam(algorithm) and validateNetworkParam(network):
        return get_predicted_graph(network, algorithm)
    else:
        return json.dumps({'error':'invalid algorithm or network'})

@app.route("/network-characteristic/betweenness-centralization/<network>")
def load_betweenness_centralization(network=None):
    print("START: load betweenness_centralization for " + network)
    if validateNetworkParam(network):
        print("OK")
        original_graph, _ = create_networkX_graphs(network)
        return make_json_object_between_cantralization(original_graph)
    else:
        return json.dumps({'error':'invalid network'})

@app.route("/network-characteristic/average-effective-size/<network>")
def load_average_effective_size(network=None):
    print("START: load average_effective_size for " + network)
    if validateNetworkParam(network):
        original_graph, _ = create_networkX_graphs(network)
        return make_json_object_avg_effective_size(original_graph)
    else:
        return json.dumps({'error':'invalid network'})

@app.route("/network-characteristic/degree-centralization/<network>")
def load_degree_centralization(network=None):
    print("START: load degree_centralization for " + network)
    if validateNetworkParam(network):
        original_graph, _ = create_networkX_graphs(network)
        return make_json_object_degree_cantralization(original_graph)
    else:
        return json.dumps({'error':'invalid network'})