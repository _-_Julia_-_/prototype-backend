from ast import operator
from locale import normalize
import networkx as nx

def density(graph):
    edges_count = len(list(graph.edges))
    nodes_count = graph.number_of_nodes()

    potential_connections_count = (nodes_count * (nodes_count-1)) / 2

    return edges_count / potential_connections_count

def degree_centralization(graph):
    node_degrees = list(dict(graph.degree()).values())
    sum_degree_centrality = calculate_centrality_sum(node_degrees)

    nodes_count = len(node_degrees)
    
    return sum_degree_centrality / ((nodes_count-1)*(nodes_count-2))

def betweenness_centralization(graph):
    node_betweenness_centralities = list(nx.betweenness_centrality(graph, normalized=False).values())
    sum_betweenness_centrality = calculate_centrality_sum(node_betweenness_centralities)

    nodes_count = len(node_betweenness_centralities)

    return sum_betweenness_centrality / ((nodes_count-1)*(nodes_count-1)*(nodes_count-2))
     
def calculate_centrality_sum(node_centralities):
    max_degree = max(node_centralities)

    sum = 0
    for node_degree in node_centralities:
        sum = sum + (max_degree - node_degree)
    
    return sum

def average_degree_centrality(graph):
    all_nodes_degrees = graph.degree

    sum_degrees = 0
    for _, degree in all_nodes_degrees:
        sum_degrees = sum_degrees + degree

    return sum_degrees / graph.number_of_nodes()

#warning: long computation time
def average_effective_size(graph):
    node_effective_sizes = nx.effective_size(graph).values()
    nodes_count = len(node_effective_sizes)

    effective_sizes_sum = 0
    for effective_size in node_effective_sizes:
        effective_sizes_sum = effective_sizes_sum + effective_size
    
    average_effective_size = effective_sizes_sum / nodes_count
    return average_effective_size