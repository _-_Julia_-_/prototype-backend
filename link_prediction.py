import networkx as nx

from helper import *
from link_prediction_algorithms import *
from json_object_building import *

from itertools import combinations, tee

def get_predicted_graph(network, algorithm):
    (initial_graph, actual_graph) = create_networkX_graphs(network)

    all_node_pairs = combinations(initial_graph.nodes, 2) 
    prediction_result = []

    # predicts only for in initial_graph non existing edges
    if algorithm == 'cn':
        prediction_result = common_neigbors(initial_graph, all_node_pairs)
    elif algorithm == 'jc':
        prediction_result = jaccards_coefficient(initial_graph)
    elif algorithm == 'pa':
        prediction_result = preferential_attachment(initial_graph)
    elif algorithm == 'aa':
        prediction_result = adamic_adar(initial_graph)
    elif algorithm == "k":
        prediction_result = katz(initial_graph, all_node_pairs, network)
    elif algorithm == "ct":
        prediction_result = commute_time(initial_graph, all_node_pairs)

    iterator1_prediction_result, iterator2_prediction_result = tee(prediction_result)

    roc_object = calculate_roc_curve_values(iterator1_prediction_result, actual_graph, False if algorithm == "ct" else True, network)
    final_graph, prediction_result_object = check_predicted_edges(iterator2_prediction_result, actual_graph, roc_object['opt_treshold'], False if algorithm == "ct" else True)
    return build_json_object_of_graph(final_graph, roc_object, prediction_result_object, network)


def calculate_roc_curve_values(prediction_result, actual_graph, max_score_relevant, network):
    decimals = 2
    if network == "primary-school":
        decimals = 4
    scores, labels = zip(*[(round(s, decimals), (u,v) in actual_graph.edges) for (u,v,s) in prediction_result])

    fprs, tprs, tresholds = roc_curve_values(labels, scores, max_score_relevant=max_score_relevant)
    auc = calculate_auc_value(fprs, tprs)

    points = zip(fprs, tprs, tresholds)
    maximum_youdens_j = (-1000, 0) # (result, treshold)
    for fpr, tpr, treshold in points:
        result = tpr - fpr
        current_maximum, _ = maximum_youdens_j
        if result > current_maximum:
            maximum_youdens_j = (result, treshold)

    return make_json_object_evaluation(fprs, tprs, auc, maximum_youdens_j, tresholds)

def create_networkX_graphs(network):
    res = []

    if network=="confluence":
        res = read_graph_file("confluence_edges", 0, 1, 5)
    
    elif network=="hospital-ward":
        res = read_graph_file("ia-hospital-ward-proximity", 0, 1, 2)
    
    elif network=="highschool-2012":
        res = read_graph_file("highschool-2012", 1, 2, 0)

    elif network=="primary-school":
        res = read_graph_file("ia-primary-school-proximity", 0, 1, 2)
    
    elif network=="sfhh-conference":
        res = read_graph_file("sfhh-conference", 1, 2, 0)

    edges_at_timestamps = res[1]
    sorted_timestamps = list(edges_at_timestamps.keys())
    sorted_timestamps.sort()
    
    timestamps_to_use = calculate_how_much_timestamps_to_use(sorted_timestamps)

    original_graph = nx.Graph()
    actual_graph = nx.Graph()

    for time_stamp, edges_list in edges_at_timestamps.items():
        if(time_stamp <= sorted_timestamps[timestamps_to_use-1]):
            original_graph.add_edges_from(edges_list)
            actual_graph.add_edges_from(edges_list)
        else:
            actual_graph.add_edges_from(edges_list)
      
    #use only undirected graphs
    return (original_graph.to_undirected(), actual_graph.to_undirected())


def check_predicted_edges(prediction_result, actual_graph, treshold, max_score_relevant):
    true_positives = 0
    false_positives = 0
    true_negatives = 0
    false_negatives = 0

    for edge in prediction_result: #only edges that were not in the initial graph are in prediction result
        (source_node, target_node, score) = edge
        if max_score_relevant:
            if score >= treshold: #predict edge
                if actual_graph.has_edge(source_node, target_node): # TP
                    actual_graph.add_edge(source_node, target_node, color='#71c02f', label=score) #green 
                    true_positives = true_positives + 1
                else: # FP
                    actual_graph.add_edge(source_node, target_node, color='#da105a', label=score) #red 
                    false_positives = false_positives + 1
            else: #dont predict edge
                if actual_graph.has_edge(source_node, target_node): # FN
                    actual_graph.add_edge(source_node, target_node, color='#fbb11e', label=score) # orange
                    false_negatives = false_negatives + 1
                else: # TN
                    true_negatives = true_negatives + 1
        else:
            if score <= treshold: #predict edge
                if actual_graph.has_edge(source_node, target_node): # TP
                    actual_graph.add_edge(source_node, target_node, color='#71c02f', label=score) #green 
                    true_positives = true_positives + 1
                else: # FP
                    actual_graph.add_edge(source_node, target_node, color='#da105a', label=score) #red 
                    false_positives = false_positives + 1
            else: #dont predict edge
                if actual_graph.has_edge(source_node, target_node): # FN
                    actual_graph.add_edge(source_node, target_node, color='#fbb11e', label=score) # orange
                    false_negatives = false_negatives + 1
                else: # TN
                    true_negatives = true_negatives + 1
    
    prediction_result_object = make_json_object_prediction_results(true_positives, true_negatives, false_positives, false_negatives)
    
    return (actual_graph, prediction_result_object)