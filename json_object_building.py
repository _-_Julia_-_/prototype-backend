import json

from network_properties import *
from helper import *

def build_final_json_response_explanation(title, subtitles_array, explanations_array):
    response = {}
    response['title'] = title
    response['subtitles'] = subtitles_array
    response['explanations'] = explanations_array
    return json.dumps(response, ensure_ascii=False)

def build_json_object_of_graph(graph, evaluation_object, prediction_result_object, network):
    network_info_object = {
        'nodes_count': len(graph.nodes),
        'edges_count': len(graph.edges),
        'background_information': read_background_information_file(network),
        'density': round(density(graph),2),
        'avg_degree_centrality': round(average_degree_centrality(graph), 2),
    }

    nodes_json_array = make_nodes_json_array_vis(graph.nodes)
    edges_json_array = make_edges_json_array_vis(graph.edges(data=True))

    result_json = {}
    result_json['info'] = network_info_object
    result_json['nodes'] = nodes_json_array
    result_json['links'] = edges_json_array
    result_json['evaluation'] = evaluation_object
    result_json['prediction_result'] = prediction_result_object
    return json.dumps(result_json)

def make_nodes_json_array_vis(nodes_list):
    json_array_nodes = []
    for n in nodes_list:
        json_object_nodes = {}
        json_object_nodes['id'] = n
        json_object_nodes['label'] = n

        color = {}
        color['border'] = "#08516f"
        color['background'] = "#095c7e"
        
        highlight = {}
        highlight['border'] = "#018d8d"
        highlight ['background'] = "#00adad"

        hover = {}
        hover['border'] = "#074c69"
        hover['background'] = "#095c7e"

        color['highlight'] = highlight
        color['hover'] = hover

        json_object_nodes['color'] = color
        
        json_array_nodes.append(json_object_nodes)

    return json_array_nodes

# create a JSON array for the links according to d3.js documentation
# https://flowingdata.com/2012/08/02/how-to-make-an-interactive-network-visualization/
def make_edges_json_array_vis(edges_list):
    json_array_edges = []
    for e_tuple in edges_list:
        (source_node, target_node, attributes) = e_tuple
        id = str(source_node)+"_"+str(target_node)

        json_object_edges = {}
        json_object_edges['id'] = id
        json_object_edges['from'] = source_node
        json_object_edges['to'] = target_node
        if 'label' in attributes:
            if not isinstance(attributes['label'], str):
                json_object_edges['title'] = str(round(attributes['label'], 2))        
        
        color = { }
        color['color'] = attributes['color']
        color['highlight']  = attributes['color']
        color['hover']  = attributes['color']
        color['opacity']  = 1.0
        
        json_object_edges['color'] = color # default:"#1e94cc" wrong prediction:"#da105a" correct prediction:"#71c02f"
        json_array_edges.append(json_object_edges)

    return json_array_edges

def make_json_object_evaluation(fpr, tpr, auc, maximum_youdens_j, tresholds):
    _, treshold = maximum_youdens_j
    evaluation_metrics_object = {}
    evaluation_metrics_object['fpr'] = [f for f in fpr]
    evaluation_metrics_object['tpr'] = [t for t in tpr]
    evaluation_metrics_object['auc'] = round(auc, 2)
    evaluation_metrics_object['opt_treshold'] = float(treshold)
    evaluation_metrics_object['tresholds'] = [float(t) for t in tresholds]
    return evaluation_metrics_object

def make_json_object_prediction_results(true_positives, true_negatives, false_positives, false_negatives):
    prediction_results = {}
    prediction_results['TP'] = int(true_positives)
    prediction_results['TN'] = int(true_negatives)
    prediction_results['FP'] = int(false_positives)
    prediction_results['FN'] = int(false_negatives)
    return prediction_results

def make_json_object_between_cantralization(graph):
    betweenness_centr = betweenness_centralization(graph)
    json_object = {
        'betweenness_centralization': round(betweenness_centr, 3)
    }
    return json.dumps(json_object)

def make_json_object_degree_cantralization(graph):
    degree_centr = degree_centralization(graph)
    json_object = {
        'degree_centralization': round(degree_centr, 2)
    }
    return json.dumps(json_object)

def make_json_object_avg_effective_size(graph):
    avg_effective_size = average_effective_size(graph)
    json_object = {
        'avg_effective_size': round(avg_effective_size, 2)
    }
    return json.dumps(json_object)