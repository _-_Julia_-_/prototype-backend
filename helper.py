import csv
import os.path
import numpy as np

def read_graph_file(name, index_edge_from, index_edge_to, index_time):
    edges_at_timestamps = {} #dict with: timestamps as keys and a list of the edges from these timestams as values
    nodes_no_duplicates = []
    try:
        current_path = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_path, "graphs", name+".csv")

        isMultigraph = False
        isDirectedGraph = False
        
        nodes = {}
        with open(file_path, 'r') as file:
            reader = csv.reader(file)
            for line in reader:
                if index_time < len(line):
                    if line[index_time] not in edges_at_timestamps:
                        edges_at_timestamps[line[index_time]] = []

                    current_edges_at_timestamp = edges_at_timestamps[line[index_time]]
                    if (line[index_edge_from], line[index_edge_to], {'color': '#1e94cc', 'label': ''}) in current_edges_at_timestamp:
                        isMultigraph = True
                    if (line[index_edge_to], line[index_edge_from], {'color': '#1e94cc', 'label': ''}) in current_edges_at_timestamp:
                        isDirectedGraph = True

                    edges_at_timestamps[line[index_time]].append((line[index_edge_from], line[index_edge_to], {'color': '#1e94cc', 'label': ''}))
                    nodes[line[index_edge_from]] = 0
                    nodes[line[index_edge_to]] = 0
                else:
                    print('error with line: ',line)

        nodes_no_duplicates = list(nodes.keys())
    except:
        print("ERROR: error reading graph file:"+ name)
    else:
        print("INFO: read graph file:"+name+" which is multigraph: "+str(isMultigraph)+" an is directedGraph: "+str(isDirectedGraph))
    return [nodes_no_duplicates, edges_at_timestamps]

def read_explanation_file(algorithm_shortcut):
    title = ""
    subtitles = []
    all_texts = []

    try:
        current_path = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_path, "explanations", algorithm_shortcut+".txt")

        section = 0
        text = ""

        file = open(file_path, 'r', encoding='utf-8')
        lines = file.readlines()

        for line in lines:
            if section == 0:
                if not line == "---\n" and not line == "---":
                    title = line
                else:
                    section = section + 1
            elif section == 1:
                if not line == "---\n" and not line == "---":
                    subtitles.append(line)
                else:
                    section = section + 1
            elif not line == "---\n" and not line == "---":
                text += line
            else:
                all_texts.append(text)
                text = ""
                section = section + 1
    except:
        print("ERROR: error reading explanation file: "+algorithm_shortcut)
    else:
        print("INFO: read explanation file: "+algorithm_shortcut)

    return (title, subtitles, all_texts)

def read_background_information_file(network):
    text = ""

    try:
        current_path = os.path.abspath(os.path.dirname(__file__))
        file_path = os.path.join(current_path, "background_informations", network+".txt")

        

        try:
            file = open(file_path, 'r', encoding='utf-8')
            lines = file.readlines()
            for line in lines:
                text += line
        except:
            text = "keine Informationen verfügbar"

    except:
        print("ERROR: error reading background information file: "+network)
    else:
        print("INFO: read background information file: "+network)

    return text

# use 2/3 of all timestamps as starting point for prediction
def calculate_how_much_timestamps_to_use(all_tiemstams_list):
    number_of_timestamps = len(all_tiemstams_list)
    timestamps_to_use = 1
    if number_of_timestamps > 2:
        timestamps_to_use = round(number_of_timestamps/3)*2

    return timestamps_to_use

def roc_curve_values(labels, scores, max_score_relevant):
    values = zip(labels, scores)
    values_list = list(values)
    sorted_list = sorted(values_list, key = lambda x: x[1])

    tresholds = set(scores)
    tresholds = sorted(tresholds, reverse=True if max_score_relevant else False)
    tresholds.append(tresholds[0]+1)
    tresholds.append(0)
    tresholds.sort(reverse=True if max_score_relevant else False)
    tprs = []
    fprs = []

    for treshold in tresholds:
        TP = 0
        FP = 0
        TN = 0
        FN = 0
        for entry in sorted_list:
            label, score = entry
            if max_score_relevant:
                if score >= treshold:
                    if label == True:
                        TP = TP + 1
                    else:
                        FP = FP + 1
                else:
                    if label == False:
                        TN = TN + 1
                    else:
                        FN = FN + 1
            else:
                if score <= treshold:
                    if label == True:
                        TP = TP + 1
                    else:
                        FP = FP + 1
                else:
                    if label == False:
                        TN = TN + 1
                    else:
                        FN = FN + 1
        
        TPR = TP / (TP + FN)
        FPR = FP / (FP + TN)

        tprs.append(TPR)
        fprs.append(FPR)
    
    return (fprs, tprs, tresholds)

def calculate_auc_value (fprs, tprs):
    elements_count = len (fprs)

    auc = 0
    for i in range (1, elements_count-1):
        length_1 = tprs[i-1]
        length_2 = tprs[i]
        height = fprs[i] - fprs[i-1]
        area = calculate_trapezium_area(length_1, length_2, height)
        auc = auc + area

    return auc

def calculate_trapezium_area (length_parallel_side_1, lenght_parallel_side_2, height):
    return ((length_parallel_side_1 + lenght_parallel_side_2) / 2) * height       

def validateAlgorithmParam(algorithmParam):
    if algorithmParam in ["cn", "jc", "pa", "aa", "k", "ct"]:
        return True
    return False

def validateNetworkParam(networkParam):
    if networkParam in ["confluence", "hospital-ward", "highschool-2012", "primary-school", "sfhh-conference"]:
        return True
    return False