# Backend
Dieses Repository enthält den Code des Backends des Educational-Software-Prototyps zum Einstieg in das Thema der Kantenvorhersage in sozialen Netzwerken. 
Der Prototyp wurde im Rahmen einer Bachelorarbeit an der TH Nürnberg Georg Simon Ohm entwickelt.
Das Backend wurde mit Python und Flask implementiert.


## Aufsetzen einer neuen Anaconda Environment
```
conda deactivate
conda create -n <environment-name> python=3.8
conda activate <environment-name>
```


### Installieren der benötigten Dependencies
```
conda install flask
conda install flask-cors
conda install networkx

conda install scikit-learn
conda install numpy
conda install colorama
```


## Backend starten
```
set FLASK_APP=backend_start
flask run [--host=0.0.0.0]
```


## Anmerkungen
Das Backend läuft per Default auf 127.0.0.1:5000

REST-Endpoints:
```
/original-network/<network>
/explanations/<algorithm>
/start-algorithm/<algorithm>/<network>
/network-characteristic/betweenness-centralization/<network>
/network-characteristic/average-effective-size/<netwok>
/network-characteristic/degree-centralization/<network>
```
