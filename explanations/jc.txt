Jaccard's Coefficient Algorithmus
---
Allgemeine Information
Schritt 0: Grundidee
Schritt 1: Nachbarn jedes Knotens herausfinden
Schritt 2: Anzahl gemeinsamer Nachbarn und aller Nachbarn für Knotenpaare ermitteln
Schritt 3: Jaccard's Coefficient für Knotenpaare berechnen
Schritt 4: Wähle die Knotenpaare mit den größten Jaccard's Coefficient
Ergebnis - Graph
Ergebnis - ROC-Kurve
---
Jaccard's Coefficient Algorithmus ist ein <b>nachbarschaftsbasierter</b> Algorithmus zur Kantenvorhersage. Er arbeitet also mit der Nachbarschaftsbeziehung von Knoten.
<br>Beim Jaccard's Coefficient Algorithmus handelt es sich um eine <b>normalisierte Version des Common Neighbors Algorithmus</b>.
<br>
<br>
<br>
<br><u>Die folgenden Schritte werden beim Jaccard's Coefficient Algorithmus ausgeführt und im Folgenden genauer erklärt:</u>
<br><b>Schritt 0</b>: Grundidee
<br><b>Schritt 1</b>: Nachbarn jedes Knotens herausfinden
<br><b>Schritt 2</b>: Anzahl gemeinsamer Nachbarn und aller Nachbarn für Knotenpaare ermitteln
<br><b>Schritt 3</b>: Jaccard's Coefficient für Knotenpaare berechnen
<br><b>Schritt 4</b>: Wähle die Knotenpaare mit den größten Jaccard's Coefficient
<br>
<br>
<br>Weitere Informationen zum Nachlesen findest du zum Beispiel 
<ul>
<li>im Kapitel 9: <a href="https://link.springer.com/content/pdf/10.1007%2F978-1-4419-8462-3.pdf#page=256">A Survey of Link Prediction in Social Networks</a> des Buchs Social Network Data Analytics</li>
</ul>
---
<br>Dem Jaccard's Coefficient Algorithmus liegt das <b>Prinzip des Clusterings</b> zugrunde:
<br>
<br>In sozialen Netzwerken gibt es einen hohen Grad an Transitivität. Daraus resultiert, dass es für zwei Teilnehmer eines sozialen Netzwerks wahrscheinlich ist, dass zwischen ihnen eine Interaktion stattfinden wird bzw. eine Beziehung entstehen wird, wenn diese beiden Teilnehmer bereits mit einem gemeinsamen dritten Teilnehmer interagieren bzw. eine Beziehung zu diesem haben.
<br>Aus dem Prinzip des Clusterings folgt daher, dass es <b>umso wahrscheinlicher</b> ist, dass in Zukunft eine <b>Kante</b> zwischen zwei Knoten <b>existieren wird</b>, <b>je mehr gemeinsame Nachbarn</b> die beiden Knoten haben.
<br>
<br>Der Jaccard's Coefficient Algorithmus entscheidet daher anhand der Anzahl der gemeinsamen Nachbarn, die zwei Knoten haben, ob zwischen diesen eine Kante entstehen wird. 
Anders als beim Common Neighbors Algorithmus wird aber nicht die reine Anzahl der gemeinsamen Nachbarn verwendet. 
Die Anzahl der gemeinsamen Nachbarn wird beim Jaccard's Coefficient Algorithmus <b>normalisiert</b> indem die <b>Anzahl der gemeinsamen Nachbarn durch die Anzahl aller Nachbarn geteilt</b> wird.
---
<b>Der erste Schritt des Jaccard's Coefficient Algorithmus identisch zum ersten Schritt der Algorithmen: Common Neighbors, Preferential Attachment, Adamic/Adar</b>
<br>
<br>Im ersten Schritt des Jaccard's Coefficient Algorithmus wird jeder Knoten betrachtet und für diesen ermittelt, mit welchen Knoten dieser Benachbart ist. 
<br>
<br>
<br>
<br><u>Im Netzwerk auf der linken Seite</u>
<br>Im von dir gewählten Netzwerk auf der linken Seite kannst du <b>herausfinden, mit welchen Knoten ein Knoten benachbart ist</b>. 
<br>Klicke dazu einen beliebigen Knoten lange an. Dadurch ...
<ul style="line-height:100%">
<li style="margin: 15px 0;"> wird der <span style='color:#00adad;'>ausgewählte Knoten türkis</span> markiert.</li> 
<li> werden die <span style='color:#CFB407;'>Nachbarn</span> des gewählten Knotens <span style='color:#CFB407;'>gelb</span> markiert.
</ul>
<br>Um die Auswahl des Knotens aufzuheben oder zu ändern klicke einfach auf eine beliebige Stelle am Hintergrund des Netzwerks.
---
Im zweiten Schritt des Jaccard's Coefficient Algorithmus wird für jedes dieser Knotenpaar (zwischen dem noch keine Kante existiert) die Anzahl ihrer gemeinsamen Nachbarn sowie die Gesamtanzahl aller Nachbarn ermittelt. 
<ul>
<li style="margin: 15px 0;"><b>Gemeinsame Nachbarn</b> zweier Knoten A und B sind alle Knoten, die sowohl mit A als auch mit B benachbart sind.</li>
<li><b>Alle Nachbarn</b> zweier Knoten A und B sind die Nachbarn von A und die Nachbarn von B zusammengenommen.</li>
</ul>
---
Im dritten Schritt des Jaccard's Coefficient Algorithmus wird für jedes Knotenpaar (zwischen dem noch keine Kante existiert) der Jaccard's Coefficient berechnet.
Dieser berechnet sich für zwei Knoten x und y wie folgt:
<br>
<br>
<b style='color:#1e94cc'>
<table>
    <tr>
        <td>
        </td>
        <td style="text-align: left;">
           GemeinsameNachbarn(x, y)
        </td>
    </tr>
    <tr>
        <td>
            Jaccard's Coefficient(x, y) =
        </td>
        <td style="border-top: 1pt solid cornflowerblue;text-align: left;">
            AlleNachbarn(x, y)</b>
        </td>
    </tr>
</table>
</b>
<br>
<br>wobei GemeinsameNachbarn(x, y) für die Anzahl der gemeinsamen Nachbarn der Knoten x und y steht
<br>und AlleNachbarn(x, y) für die Anzahl aller Nachbarn der Knoten x und y steht.
<br>
<br>
<br>
<br><u>Im Netzwerk auf der linken Seite</u>
<br>Im von dir gewählten Netzwerk auf der linken Seite kannst du den <b>Jaccard's Coefficient für zwei beliebige Knoten ermitteln</b>.
<br>Dafür musst du zwei Knoten mit einem langen Klick auswählen. Dann werden die Knoten wie folgt dargestellt:
<ul style="line-height:100%">
<li style="margin: 15px 0;"> Die <span style='color:#00adad;'>gewählten Knoten</span> werden <span style='color:#00adad;'>türkis</span> hervorgehoben.</li>
<li style="margin: 15px 0;"> Die <span style='color:#CFB407;'>Nachbarn des ersten ausgewählten Knotens</span> werden <span style='color:#CFB407;'>gelb</span> markiert.</li>
<li style="margin: 15px 0;"> Die <span style='color:#740976;'>Nachbarn des zweiten ausgewählten Knotens</span> werden durch eine <span style='color:#740976;'>lilane Umrandung</span> markiert.</li>
<li style="margin: 15px 0;"> <b>Gemeinsame Nachbarn</b> der zwei gewählten Knoten sind dann alle <b>orangenen Knoten, die eine grüne Umrandung haben</b>.</li>
<li> <b>Alle Nachbarn</b> der zwei gewählten Knoten sind dann alle <b>orangenen Knoten</b> sowie alle <b>Knoten, die eine grüne Umrandung haben</b>.</li>
</ul>
<br>Der Jaccard's Coefficient für die beiden gewählten Knoten und wie er berechnet wird wird in Textform unterhalb des Graphs angezeigt.
<br>
<br>Um die Auswahl der Knoten aufzuheben oder zu ändern klicke einfach auf eine beliebige Stelle am Hintergrund des Netzwerks.
---
Im vierten und finalen Schritt des Jaccard's Coefficient Algorithmus werden die Knotenpaare bestimmt, für die vorhergesagt wird, dass zwischen ihnen eine Kante entstehen wird.
<br>
<br>Für das Vorhersagen der entstehenden Kanten wird ein <b>Schwellwert (engl. "threshold")</b> benötigt. Beim Jaccard's Coefficient Algorithmus steht dieser Schwellwert für den <b>Jaccard's Coefficient</b>, den ein Knotenpaar <b>mindestens</b> haben muss, damit der Algorithmus die Entstehung einer Kante vorhersagt. 
<br>Das bedeutet für einen Schwellwert = X:
<ul style="line-height:100%">
<li style="margin: 15px 0;"> Hat ein Knotenpaar <b>mindestens einen Jaccard's Coefficient von X</b>, dann sagt der Algorithmus die <b>Entstehung einer Kante</b> zwischen diesen beiden Knoten vorher.</li>
<li> Hat ein Knotenpaar einen<b>Jaccard's Coefficient &lt; X</b>, dann sagt der Algorithmus vorher, dass <b>keine Kante</b> zwischen diesen beiden Knoten entstehen wird.</li>
</ul>
---
Für das von dir ausgewählte Netzwerk sieht das vorhergesagte Netzwerk aus wie auf der linken Seite dargestellt.
<br>
<br>
<br>
<br><u>Im Netzwerk auf der linken Seite</u>
<br>Die Kantenfarben bedeuten folgendes:
<ul style="line-height:100%">
<li style='color:#1e94cc;margin: 15px 0;'>blau = ursprüngliches Netzwerk</li>
<li style='color:yellowgreen;margin: 15px 0;'>grün = korrekt vorhergesagte Kante ("true positive" - TP)</li>
<li style='color:crimson;margin: 15px 0;'>rot = Kante wurde vorhergesagt, existiert jedoch nicht ("false positive" - FP)</li>
<li style='color:orange;'>orange = Kante wurde nicht vorhergesagt, würde jedoch existieren ("false negative" - FN))</li>
</ul>
<br>Durch <b>Hovern</b> über die rot, grün oder orangenen Kanten bekommst du den <b>Jaccard's Coefficient für das Knotenpaar</b>, das durch die Kante verbunden wird, angezeigt. Wenn du diese Zahl mit dem angezeigten Threshold vergleichst, der unter dem Netzwerk angezeigt wird, kannst du nachvollziehen, warum der Algorithmus die Entstehung der Kante vorhergesagt hat oder nicht.
---
Wie gut die Kantenvorhersage mit dem ausgewählten Algorithmus auf dem gewählten Netzwerk funktioniert lässt sich gut mit Hilfe einer <b>ROC-Kurve</b> und dem <b>AUC-Wert</b> veranschaulichen. 
<br>
<br>
<br>
<br><u>Im Diagramm auf der linken Seite</u>
<br>ist die ROC-Kurve für die Kantenvorhersagen mit dem Jaccard's Coefficient Algorithmus auf dem von dir ausgewählten Netzwerk abgebildet.
<br>Durch hovern über einen Punkte der ROC-Kurve (dort, wo "Knicke" in der Kurve sind) werden dir drei Werte für diesen Punkt angezeigt: <b>FPR, TPR</b> und der dazugehörige <b>Threshold</b>.
<br>Das ganze liest sich für die Beispielwerte <b>FPR = a, TPR = b und Threshold t</b> wie folgt: <b>"Bei einem Threshold von t erreicht der Jaccard's Coefficient Algorithmus eine TPR von b und eine FPR von a."</b>
<br>
<br>Unter dem Diagramm wird sowohl der AUC-Wert angezeigt wie auch der optimale Threshold, der mit Hilfe der ROC-Kurve ermittelt wurde.
---