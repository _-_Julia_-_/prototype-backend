import networkx as nx
import numpy as np
import numpy.matlib as matlib
import math

# all predictors predict only for in initial_graph non existing edges

def common_neigbors(graph, nodepairs):
    common_neighbors_result = []

    for (source, target) in nodepairs:
        if not graph.has_edge(source, target):
           common_neighbors_result.append((source, target, len(list(nx.common_neighbors(graph, source, target)))))
    
    return common_neighbors_result

def jaccards_coefficient(graph):
    result = nx.jaccard_coefficient(graph) #returns iterator of 3-tuples in the form (u, v, p) where (u, v) is a pair of nodes and p is their Jaccard coefficien
    jaccard_rounded = []
    for source, target, score in result:
        score = round(score, 2)
        jaccard_rounded.append((source, target, score))
    return jaccard_rounded

def preferential_attachment(graph):
    return nx.preferential_attachment(graph) #returns iterator of 3-tuples in the form (u, v, p) where (u, v) is a pair of nodes and p is their Jaccard coefficien

def adamic_adar(graph):
    result = nx.adamic_adar_index(graph) #returns iterator of 3-tuples in the form (u, v, p) where (u, v) is a pair of nodes and p is their Jaccard coefficien
    adamic_adar_rounded = []
    for source, target, score in result:
        score = round(score, 2)
        adamic_adar_rounded.append((source, target, score))
    return adamic_adar_rounded

def katz(graph, nodepairs, network):
    katz_scores = []

    #networkx.convert_matrix.to_numpy_matrix: if nodelist is None, then the ordering is produced by G.nodes()
    nodes_order = list(graph.nodes)
    nodes_count = len(nodes_order)

    adjacency_matrix = nx.to_numpy_matrix(graph) #unwightened: 1 if edge exists, 0 if no edge exists between nodes
    identity_matrix = np.identity(nodes_count)
    beta = 0.05
    if network == "primary-school":
        beta = 0.005

    #formula according to Hasan 2011 and Liben-Nowell und Kleinberg
    katz_matrix = np.linalg.inv(identity_matrix - (beta * adjacency_matrix)) - identity_matrix
    for source, target in nodepairs:
        if not graph.has_edge(source, target):
            source_index = nodes_order.index(source)
            target_index = nodes_order.index(target)
            katz_score = katz_matrix[source_index, target_index]
            katz_scores.append((source, target, round(katz_score, 3)))
    
    return katz_scores

def commute_time(graph, nodepairs):
    commute_time_result = []

    edges_count = graph.number_of_edges()
    nodes_order = list(graph.nodes())

    adjacency_matrix = nx.to_numpy_matrix(graph)
    diagonal_matrix = calculate_diagonal_matrix_commute_time(graph)
    matrix_l = diagonal_matrix - adjacency_matrix
    pseudo_inverse_matrix = np.linalg.pinv(matrix_l)

    for source, target in nodepairs:
        if not graph.has_edge(source, target):
            source_index = nodes_order.index(source)
            target_index = nodes_order.index(target)
            #formula according to Wang et al. 2015
            commute_time = edges_count * (pseudo_inverse_matrix[source_index, source_index] + pseudo_inverse_matrix[target_index, target_index] - (2 * pseudo_inverse_matrix[source_index, target_index]))
            #commute_time_result.append((source, target, commute_time))
            commute_time_result.append((source, target, round(commute_time, 1)))

    return commute_time_result

def calculate_diagonal_matrix_commute_time(graph):
    nodes_count = graph.number_of_nodes()
    adjacency_matrix = nx.to_numpy_matrix(graph)
    diagonal_matrix = matlib.zeros((nodes_count, nodes_count))

    for i in range (0, nodes_count):
        sum = 0
        for j in range (0, nodes_count):
            sum = sum + adjacency_matrix[i, j]
        diagonal_matrix[i, i] = sum
    
    return diagonal_matrix